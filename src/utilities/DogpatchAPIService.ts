export interface Complexity {
  id: number;
  complexity: string;
  complex_code: string;
  description: string;
  examples: string;
}

export interface PlayStyles {
  id: number;
  name: string;
  code: string;
  description: string;
}

export interface PlayTime {
  id: number;
  time_string: string;
  code: number;
}

export interface PlayType {
  id: number;
  play_type: string;
  type_code: string;
  description: string;
  examples: string;
}

export interface Theme {
  id: number;
  name: string;
  code: string;
  description: string;
}

export interface Item {
  id: number;
  name: string;
  code: string;
  description: string;
}

export class DogpatchAPIService {
  constructor() {}

  baseURL: string = "http://localhost:3000";

  getComplexities(): Promise<Complexity[]> {
    // For now, consider the data is stored on a static `users.json` file
    return (
      fetch(this.baseURL + "/complexities")
        // the JSON body is taken from the response
        .then((res) => res.json())
        .then((res) => {
          // The response has an `any` type, so we need to cast
          // it to the `User` type, and return it from the promise

          // TODO: THIS IS CLEARLY NOT WORKING AS EXPECTED. NEED TO LOOK INTO THIS. WHY IS IT NOT CASTING THE VALUES TO THE SET INTERFACE?
          return res as Complexity[];
        })
    );
  }

  getPlayStyles(): Promise<PlayStyles[]> {
    // For now, consider the data is stored on a static `users.json` file
    return (
      fetch(this.baseURL + "/playStyles")
        // the JSON body is taken from the response
        .then((res) => res.json())
        .then((res) => {
          // The response has an `any` type, so we need to cast
          // it to the `User` type, and return it from the promise
          return res as PlayStyles[];
        })
    );
  }

  getPlayTimes(): Promise<PlayTime[]> {
    // For now, consider the data is stored on a static `users.json` file
    return (
      fetch(this.baseURL + "/playTimes")
        // the JSON body is taken from the response
        .then((res) => res.json())
        .then((res) => {
          // The response has an `any` type, so we need to cast
          // it to the `User` type, and return it from the promise
          return res as PlayTime[];
        })
    );
  }

  getPlayTypes(): Promise<PlayType[]> {
    // For now, consider the data is stored on a static `users.json` file
    return (
      fetch(this.baseURL + "/playTypes")
        // the JSON body is taken from the response
        .then((res) => res.json())
        .then((res) => {
          // The response has an `any` type, so we need to cast
          // it to the `User` type, and return it from the promise
          return res as PlayType[];
        })
    );
  }

  getThemes(): Promise<Theme[]> {
    // For now, consider the data is stored on a static `users.json` file
    return (
      fetch(this.baseURL + "/themes")
        // the JSON body is taken from the response
        .then((res) => res.json())
        .then((res) => {
          // The response has an `any` type, so we need to cast
          // it to the `User` type, and return it from the promise
          return res as Theme[];
        })
    );
  }
}
